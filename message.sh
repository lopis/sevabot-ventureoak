#!/bin/bash

chat=$1
msg=$2
secret="matutatu"
msgaddress="http://localhost:5000/msg/"
chatsaddress="http://localhost:5000/chats/"

chat=$(curl -s --form "secret=matutatu" $chatsaddress | grep "$chat" | awk '{split($0,a,"|"); print a[2]}' | tail -1) 
md5=`echo -n "$chat$msg$secret" | md5sum`

#md5sum prints a '-' to the end. Let's get rid of that.
for m in $md5; do
    break
done
curl $msgaddress --data-urlencode chat="$chat" --data-urlencode msg="$msg" --data-urlencode md5="$m"

